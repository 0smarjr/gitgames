from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth import authenticate, login as auth_login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.contrib.auth.models import User
from django.template import loader
from .forms import SignUpForm, JogoForm
from .models import Jogo, Comentario, Genero

@csrf_protect
def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            auth_login(request, user)
            return redirect('userpage')
    else:
        form = SignUpForm()
    return render(request, 'gitjogos/signup.html', {'form': form})

def login(request):
    return render(request, "gitjogos/login.html")

@csrf_protect
def submit(request):
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            auth_login(request, user)
            return redirect('/')
        else:
            messages.error(request, 'Usuário e/ou senha inválidos.')
            return redirect('/login')

def user_logout(request):
    logout(request)
    return redirect('/')

def home(request):
    recent_games = Jogo.objects.all().order_by('-dt_inicio')[:3]
    categoria = Genero.objects.all()
    return render(request, "gitjogos/home.html", {"categoria": categoria, "recent_games": recent_games})

def about_us(request):
    return render(request, "gitjogos/about_us.html")

@login_required(login_url='login')
def user_page(request):
    jogo = Jogo.objects.filter(autor=request.user)
    return render(request, "gitjogos/userpage.html", {'jogo':jogo})

def game_page(request, id):
    comentarios = Comentario.objects.filter(jogo=id)
    jogo = Jogo.objects.get(id=id)
    return render(request, 'gitjogos/gamepage.html',{'jogo':jogo, 'comentarios':comentarios})

@login_required(login_url='login')
def submit_comentario(request, id):
    comentario = request.POST.get('comentario')
    jogo = Jogo.objects.get(id=id)
    usuario = request.user
    c = Comentario.objects.create(jogo=jogo,usuario=usuario,comentario=comentario)
    url = '/game/{}/'.format(id)
    return redirect(url)

@login_required(login_url='login')
def game_register(request):
    form = JogoForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        jogo = form.save(commit=False)
        jogo.autor = request.user
        jogo.save()
        return redirect('userpage')
    return render(request, 'gitjogos/gameregister.html',{'form':form})

@login_required(login_url='login')
def delete_game(request, id):
    jogo = Jogo.objects.get(id=id)
    if jogo.autor == request.user:
        jogo.delete()
    return redirect('userpage')

def edit_game(request, id):
    jogo = get_object_or_404(Jogo, pk=id)
    form = JogoForm(request.POST or None, request.FILES or None, instance=jogo)
    if form.is_valid():
        jogo=form.save(commit=False)
        jogo.autor = request.user
        jogo.save()
        url = '/game/{}/'.format(id)
        return redirect(url)
    return render(request, 'gitjogos/gameregister.html',{'form':form})

@login_required(login_url='login')
def delete_comentario(request, idjogo, idcomentario):
    comentario = Comentario.objects.get(id=idcomentario)
    if comentario.usuario == request.user:
        comentario.delete()
    url = '/game/{}/'.format(idjogo)
    return redirect(url)

def categorias(request):
    categoria = Genero.objects.all()
    gambiarra = {}
    for id_cat in categoria:
        jogos = [joguinho for joguinho in Jogo.objects.filter(genero=id_cat)[:5]]
        print(jogos, id_cat)
        gambiarra.update({id_cat.nome: jogos})
    print(gambiarra)
    return render(request, 'gitjogos/categorias.html', {'categoria': categoria, "gambiarra": gambiarra})

def pagina_categoria(request, name):
    id = Genero.objects.get(nome=name).id
    jogos = Jogo.objects.filter(genero=id)
    categoria = Genero.objects.get(id=id)
    return render(request, 'gitjogos/pagina_categoria.html', {'jogos':jogos, 'categoria':categoria})

def other_user_page(request, name):
    id = User.objects.get(username=name).id
    usuario = User.objects.get(username=name)
    jogo = Jogo.objects.filter(autor=id)
    return render(request, "gitjogos/otheruserpage.html", {'jogo':jogo, 'usuario':usuario})