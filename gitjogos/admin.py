from django.contrib import admin
from .models import Genero
from .models import Jogo, Comentario

@admin.register(Genero)
class GeneroAdmin(admin.ModelAdmin):
	list_display = ['id', 'nome', 'ativo']

@admin.register(Jogo)
class JogoAdmin(admin.ModelAdmin):
	list_display = ['id', 'nome', 'autor', 'genero', 'dt_inicio']

@admin.register(Comentario)
class ComentarioAdmin(admin.ModelAdmin):
	list_display = ['id', 'jogo', 'usuario', 'comentario']