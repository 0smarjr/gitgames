from django.urls import path

from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('login', views.login, name='login'),
    path('submit', views.submit, name='submit'),
    path('about_us', views.about_us, name='about_us'),
    path('logout', views.user_logout, name='logout'),
    path('signup', views.signup, name='signup'),
    path('userpage', views.user_page, name='userpage'),
    path('userpage/<name>', views.other_user_page, name='other_user_page'),
    path('game/<id>/', views.game_page),
    path('game/<id>/submit', views.submit_comentario),
    path('game/register', views.game_register, name='gameregister'),
    path('game/delete/<id>', views.delete_game),
    path('game/edit/<int:id>', views.edit_game, name='game_edit'),
    path('game/<idjogo>/delete_comentario/<idcomentario>', views.delete_comentario),
    path('categorias', views.categorias, name='categorias'),
    path('categorias/<name>', views.pagina_categoria),
]

