from django.apps import AppConfig


class GitjogosConfig(AppConfig):
    name = 'gitjogos'
