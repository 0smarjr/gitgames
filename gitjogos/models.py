from django.db import models
from django.contrib.auth.models import User

class Genero(models.Model):
	nome = models.CharField(max_length=100)
	ativo = models.BooleanField(default=True)

	def __str__(self):
		return str(self.nome)

	class Meta:
		db_table = 'genero'

class Jogo(models.Model):
	nome = models.CharField(max_length=100)
	autor = models.ForeignKey(User, on_delete=models.CASCADE)
	descricao = models.TextField()
	genero = models.ForeignKey(Genero, on_delete=models.CASCADE)
	icone =models.ImageField(upload_to='icones')
	dt_inicio = models.DateTimeField(auto_now_add=True)
	repositorio = models.CharField(max_length=100)
	executavel = models.FileField(upload_to='exe')

	def __str__(self):
		return str(self.nome)

	class Meta:
		db_table = 'jogo'

class Comentario(models.Model):
	jogo = models.ForeignKey(Jogo, on_delete=models.CASCADE)
	usuario = models.ForeignKey(User, on_delete=models.CASCADE)
	comentario = models.TextField()
	data = models.DateTimeField(auto_now_add=True)


	class Meta:
		db_table = 'comentario'

	def __str__(self):
		return str(self.comentario)